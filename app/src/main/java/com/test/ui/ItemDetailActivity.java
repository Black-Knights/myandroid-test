package com.test.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.test.R;

public class ItemDetailActivity extends AppCompatActivity implements View.OnClickListener{

    private RelativeLayout rlTitle;
    private TextView txTitle;
    private ImageView imgClear;
    private LinearLayout llIDKEY;
    private TextView txTagId;
    private TextView txKey;
    private LinearLayout llSwitch;
    private RelativeLayout rlMRAID;
    private Switch switch1;
    private RelativeLayout rlInChina;
    private Switch switch2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail2);
        findViews();
    }





    /**
     * Find the Views in the layout<br />
     * <br />
     * Auto-created on 2019-03-21 07:25:09 by Android Layout Finder
     * (http://www.buzzingandroid.com/tools/android-layout-finder)
     */
    private void findViews() {
        rlTitle = (RelativeLayout)findViewById( R.id.rl_title );
        txTitle = (TextView)findViewById( R.id.tx_title );

        String  name = getIntent().getStringExtra("name");
        txTitle.setText(name);

        imgClear = (ImageView)findViewById( R.id.img_clear );
        llIDKEY = (LinearLayout)findViewById( R.id.ll_ID_KEY );
        txTagId = (TextView)findViewById( R.id.tx_tag_id );
        txKey = (TextView)findViewById( R.id.tx_key );
        llSwitch = (LinearLayout)findViewById( R.id.ll_switch );
        rlMRAID = (RelativeLayout)findViewById( R.id.rl_MRAID );
        switch1 = (Switch)findViewById( R.id.switch1 );
        rlInChina = (RelativeLayout)findViewById( R.id.rl_In_China );
        switch2 = (Switch)findViewById( R.id.switch2 );
    }



    @Override
    public void onClick(View v) {
        if ( v==null ) {
            // Handle clicks for btMid
        } else if (v==null ) {
            // Handle clicks for btDemoUnits
        }else if ( v==null ) {
            Intent intent = new Intent(this,AdFormatsActivity.class);
            startActivity(intent);

            // Handle clicks for btDemoUnits
        }
    }
}
