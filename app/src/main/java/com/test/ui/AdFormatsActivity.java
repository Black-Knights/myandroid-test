package com.test.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.test.R;
import com.test.data.ListData;
import java.util.ArrayList;



public class AdFormatsActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_formats);



        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2,GridLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(gridLayoutManager);


        MyRecyclerViewAdpter myRecyclerViewAdpter = new MyRecyclerViewAdpter(R.layout.recycler_view_item);

        recyclerView.setAdapter(myRecyclerViewAdpter);


        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("TANDEM");
        stringArrayList.add("GYROSCOPE");
        stringArrayList.add("DYNAMIC");
        stringArrayList.add("MOODBOARD");
        stringArrayList.add("PARALLAX");
        stringArrayList.add("HIDOENSTORIES");
        stringArrayList.add("INFINITVCANVAS");
        stringArrayList.add("BACKGROUND VIDEO WINDOW");
        stringArrayList.add("SCROLLING BANNER");
        stringArrayList.add("UPSIDE DOWN");

        ArrayList<ListData> listDataList= new ArrayList<>();
        for(String str : stringArrayList){
            ListData listData = new ListData();
            listData.setName(str);
            listData.setImageID(R.drawable.item_image);
            listDataList.add(listData);
        }

        myRecyclerViewAdpter.setNewData(listDataList);

        myRecyclerViewAdpter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ListData listData = (ListData) adapter.getItem(position);
                Intent intent = new Intent(AdFormatsActivity.this,ItemDetailActivity.class);
                intent.putExtra("name",listData.getName());
                startActivity(intent);
            }
        });

    }

    private class MyRecyclerViewAdpter extends BaseQuickAdapter<ListData, BaseViewHolder> implements BaseQuickAdapter.OnItemClickListener {

        public MyRecyclerViewAdpter(int layoutResId) {
            super(layoutResId);
        }

        @Override
        protected void convert(BaseViewHolder helper, ListData item) {

            helper.setText(R.id.item_text,item.getName());
            helper.setImageResource(R.id.item_image,item.getImageID());



        }

        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

        }
    }
}
