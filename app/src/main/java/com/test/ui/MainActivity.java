package com.test.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.test.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button btMid;
    private Button btDemoUnits;
    private Button previews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
    }



    /**
     * Find the Views in the layout<br />
     * <br />
     * Auto-created on 2019-03-21 03:12:53 by Android Layout Finder
     * (http://www.buzzingandroid.com/tools/android-layout-finder)
     */
    private void findViews() {
        btMid = (Button)findViewById( R.id.bt_mid );
        btDemoUnits = (Button)findViewById( R.id.bt_demo_units );
        previews = (Button)findViewById( R.id.previews );

        btMid.setOnClickListener( this );
        btDemoUnits.setOnClickListener( this );
        previews.setOnClickListener( this );
    }

    /**
     * Handle button click events<br />
     * <br />
     * Auto-created on 2019-03-21 03:12:53 by Android Layout Finder
     * (http://www.buzzingandroid.com/tools/android-layout-finder)
     */
    @Override
    public void onClick(View v) {
        if ( v == btMid ) {
            // Handle clicks for btMid
        } else if ( v == btDemoUnits ) {
            // Handle clicks for btDemoUnits
        }else if ( v == previews ) {
            Intent intent = new Intent(this,AdFormatsActivity.class);
            startActivity(intent);

            // Handle clicks for btDemoUnits
        }
    }

}
